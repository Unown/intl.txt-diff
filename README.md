# Readme

This tool is used to find out where the game's original texts have been changed and the corresponding translations
need to be updated.\
It takes two `intl.txt` files, and outputs the differences between two files' original texts while ignoring all
translated texts.

It is available here: <https://unown.gitlab.io/intl.txt-diff/>.