#!/usr/bin/env sh

set -e

if [ -x public ]; then
    rm -r public
fi

mkdir public

cp index.html public/
mkdir -p public/lib/requirejs
cp lib/requirejs/require.min.js public/lib/requirejs/

tsc --build lib/intl_diff/tsconfig.json
cp lib/intl_diff/dist/intl_diff.js public/

tsc --build lib/rendering_changes/tsconfig.json
cp lib/rendering_changes/dist/rendering_changes.js public/