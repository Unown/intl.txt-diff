import { DictEntity } from "../intl/types";

import { EntityChange, Action } from "./types";
import { getBIfANotEqualToB, findBoundaryIndex, stableSort } from "./utils";

export function compareDictEntities(previousEntities: DictEntity[],
    currentEntities: DictEntity[], comparesTranslations: boolean): EntityChange[] {

    const currentEntityMap = new Map<string, DictEntity>();

    for (const currentEntity of currentEntities) {
        currentEntityMap.set(currentEntity.originalText, currentEntity);
    }

    const changes: EntityChange[] = [];
    const removedEntities: EntityChange[] = [];

    for (const previousEntity of previousEntities) {
        const currentEntity = currentEntityMap.get(previousEntity.originalText);
        if (currentEntity !== undefined) {
            if (comparesTranslations
                && previousEntity.translatedText != currentEntity.translatedText) {
                changes.push(buildEntityChange({
                    previousEntity: previousEntity,
                    currentEntity: currentEntity,
                    comparesTranslations: comparesTranslations,
                }));
            }
            currentEntityMap.delete(currentEntity.originalText);
        } else {
            removedEntities.push(buildEntityChange({
                previousEntity: previousEntity,
                comparesTranslations: comparesTranslations,
            }))
        }
    }

    const addedEntities =
        Array.from(currentEntityMap.values())
            .sort((a, b) => {
                return a.lineNumberInSection - b.lineNumberInSection;
            })

    let insertions: { afterIndex: number, entity: EntityChange, isPrevious: boolean }[] = [];
    for (const removedEntity of removedEntities) {
        insertions.push({
            afterIndex: findBoundaryIndex(changes,
                (x) => x.previous!.lineNumberInSection < removedEntity.previous!.lineNumberInSection
            ) - 1,
            entity: removedEntity,
            isPrevious: true,
        });
    }

    // an array of objects contains current line numbers
    // of modified entities with index of those entities in changes array,
    // and sorted by line number
    const something =
        Array.from(changes
            .map((change) => change.current!.lineNumberInSection)
            .entries())
            .map(([index, lineNumber]) => ({ index, lineNumber }))
            .sort((a, b) => a.lineNumber - b.lineNumber);
    for (const _addedEntity of addedEntities) {
        const addedEntity = buildEntityChange({
            currentEntity: _addedEntity,
            comparesTranslations: comparesTranslations,
        });
        const boundaryIndex = findBoundaryIndex(something,
            (x) => x.lineNumber < addedEntity.current!.lineNumberInSection
        );
        if (boundaryIndex === 0) {
            insertions.push({ afterIndex: -1, entity: addedEntity, isPrevious: false })
        } else {
            insertions.push({
                afterIndex: something[boundaryIndex - 1].index,
                entity: addedEntity,
                isPrevious: false,
            })
        }
    }

    insertions = stableSort(insertions, (a, b) => {
        if (a.afterIndex != b.afterIndex) {
            return a.afterIndex - b.afterIndex;
        }
        return Number(b.isPrevious) - Number(a.isPrevious);
    });

    let insertionOffset = 0;
    for (let i = 0; i < insertions.length;) {
        let j = i + 1;
        for (;
            j < insertions.length
            && insertions[j].afterIndex === insertions[i].afterIndex;
            j++);
        changes.splice(
            insertionOffset + insertions[i].afterIndex + 1, 0,
            ...insertions.slice(i, j).map((x) => x.entity)
        );
        insertionOffset += (j - i);
        i = j;
    }

    return changes;

}

function buildEntityChange(
    { previousEntity = null, currentEntity = null,
        comparesTranslations }
        : {
            previousEntity?: DictEntity | null;
            currentEntity?: DictEntity | null;
            comparesTranslations: boolean;
        }): EntityChange {

    if (previousEntity !== null && currentEntity !== null) {
        if (comparesTranslations) {
            return {
                action: Action.Modified,
                previous: {
                    lineNumberInSection: previousEntity.lineNumberInSection,
                    originalText: previousEntity.originalText,
                    translatedText: comparesTranslations
                        ? getBIfANotEqualToB(previousEntity.originalText, previousEntity.translatedText)
                        : undefined,
                },
                current: {
                    lineNumberInSection: currentEntity.lineNumberInSection,
                    originalText: currentEntity.originalText,
                    translatedText: comparesTranslations
                        ? getBIfANotEqualToB(currentEntity.originalText, currentEntity.translatedText)
                        : undefined,
                },
            }
        } else {
            throw new Error("unreachable");
        }
    } else if (previousEntity != null) {
        return {
            action: Action.Removed,
            previous: {
                lineNumberInSection: previousEntity.lineNumberInSection,
                originalText: previousEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(previousEntity.originalText, previousEntity.translatedText)
                    : undefined,
            },
        };
    } else if (currentEntity != null) {
        return {
            action: Action.Added,
            current: {
                lineNumberInSection: currentEntity.lineNumberInSection,
                originalText: currentEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(currentEntity.originalText, currentEntity.translatedText)
                    : undefined,
            },
        };
    }
    throw new Error("unreachable");
}