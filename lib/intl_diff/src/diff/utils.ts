export function getBIfANotEqualToB<T>(a: T, b: T): T | null {
    if (a === b) {
        return null;
    }
    return b;
}

/**
 * 
 * @param array an ordered array
 * @param sideFn a function that returns true/false when 
 *  value is on left/right side
 * 
 * @returns index of first value on right side (
 *  length of array if no value on right side)
 * 
 */
export function findBoundaryIndex<T>(array: T[], sideFn: (value: T) => boolean): number {
    let l = 0, r = array.length;
    while (true) {
        if (l === r) {
            return l;
        }
        let p = l + ((r - l) >> 1);
        switch (sideFn(array[p])) {
            case true:
                l = p + 1;
                break;
            case false:
                r = p;
                break;
        }
    }
}

// https://stackoverflow.com/a/48660568
export const stableSort = <T>(arr: T[], compare: (a: T, b: T) => number) => arr
    .map((item, index) => ({ item, index }))
    .sort((a, b) => compare(a.item, b.item) || a.index - b.index)
    .map(({ item }) => item)