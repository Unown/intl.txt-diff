import { Section, SectionType, ArrayEntity, DictEntity } from "../intl/types";

import { INTLChanges, SectionChanges, Action } from "./types";
import { compareArrayEntities } from "./array-sections";
import { compareDictEntities } from "./dict-sections";

export function compareSectionLists(previousSections: Section[],
    currentSections: Section[], comparesTranslations: boolean): INTLChanges {

    const currentSectionMap = new Map<string, Section>();
    for (const currentSection of currentSections) {
        currentSectionMap.set(currentSection.id, currentSection);
    }

    const removedSections: SectionChanges[] = [];
    const addedSections: SectionChanges[] = [];
    const modifiedSections: SectionChanges[] = [];

    for (const previousSection of previousSections) {

        const currentSection = currentSectionMap.get(previousSection.id);
        if (currentSection !== undefined) {
            currentSectionMap.delete(currentSection.id);

            const sectionChanges = previousSection.type === SectionType.ArraySection
                ? compareArrayEntities(<ArrayEntity[]>previousSection.entities, <ArrayEntity[]>currentSection.entities, comparesTranslations)
                : compareDictEntities(<DictEntity[]>previousSection.entities, <DictEntity[]>currentSection.entities, comparesTranslations);
            if (sectionChanges.length > 0) {
                modifiedSections.push({
                    id: previousSection.id,
                    type: previousSection.type,
                    action: Action.Modified,
                    previousHasTranslations: hasTranslation(previousSection, comparesTranslations),
                    currentHasTranslations: hasTranslation(currentSection, comparesTranslations),
                    previousLineNumber: previousSection.lineNumber,
                    currentLineNumber: currentSection.lineNumber,
                    changes: sectionChanges,
                });
            }
        } else {
            removedSections.push({
                id: previousSection.id,
                type: previousSection.type,
                action: Action.Removed,
                previousHasTranslations: hasTranslation(previousSection, comparesTranslations),
                previousLineNumber: previousSection.lineNumber,
            });
        }
    }

    for (const currentSection of currentSectionMap.values()) {
        addedSections.push({
            id: currentSection.id,
            type: currentSection.type,
            action: Action.Added,
            currentHasTranslations: hasTranslation(currentSection, comparesTranslations),
            currentLineNumber: currentSection.lineNumber,
        });
    }

    return {
        removedSections: removedSections,
        addedSections: addedSections,
        modifiedSections: modifiedSections,
    };
}

function hasTranslation(section: Section, comparesTranslations: boolean): boolean | undefined {
    if (!comparesTranslations) {
        return undefined;
    }
    for (const entity of section.entities) {
        if (entity.originalText != entity.translatedText) {
            return true;
        }
    }
    return false;
}