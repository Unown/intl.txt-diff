import { SectionType } from "../intl/types";

export interface INTLChanges {
    removedSections: SectionChanges[];
    addedSections: SectionChanges[];
    modifiedSections: SectionChanges[];
}

export interface SectionChanges {
    id: string;
    type: SectionType;

    action: Action;

    previousHasTranslations?: boolean;
    currentHasTranslations?: boolean;

    previousLineNumber?: number;
    currentLineNumber?: number;

    changes?: EntityChange[];
}

export interface EntityChange {
    index?: number;

    action: Action;

    previous?: Entity;
    current?: Entity;
}

export interface Entity {
    lineNumberInSection: number;

    originalText: string;
    // undefined: irrelevant; null: not translated
    translatedText?: string | null;
}

export enum Action {
    Added = "added", Removed = "removed", Modified = "modified"
}