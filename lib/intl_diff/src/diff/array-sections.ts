import { ArrayEntity } from "../intl/types";

import { EntityChange, Action } from "./types";
import { getBIfANotEqualToB } from "./utils";

export function compareArrayEntities(previousEntities: ArrayEntity[],
    currentEntities: ArrayEntity[], comparesTranslations: boolean): EntityChange[] {

    const changes: EntityChange[] = [];

    let [previousI, currentI] = [0, 0];

    while (previousI < previousEntities.length && currentI < currentEntities.length) {
        const previousEntity = previousEntities[previousI];
        const currentEntity = currentEntities[currentI];

        switch (true) {
            case previousEntity.index < currentEntity.index:
                changes.push(buildEntityChange({
                    previousEntity: previousEntity,
                    comparesTranslations: comparesTranslations
                }));
                previousI++;
                break;
            case previousEntity.index > currentEntity.index:
                changes.push(buildEntityChange({
                    currentEntity: currentEntity,
                    comparesTranslations: comparesTranslations
                }));
                currentI++;
                break;
            default:
                if (previousEntity.originalText != currentEntity.originalText
                    || (comparesTranslations && previousEntity.translatedText != currentEntity.translatedText)) {
                    changes.push(buildEntityChange({
                        previousEntity: previousEntity,
                        currentEntity: currentEntity,
                        comparesTranslations: comparesTranslations,
                    }));
                }
                previousI++;
                currentI++;
        }
    }

    if (previousI < previousEntities.length) {
        for (let i = previousI; i < previousEntities.length; i++) {
            changes.push(buildEntityChange({
                previousEntity: previousEntities[i],
                comparesTranslations: comparesTranslations
            }));
        }
    } else if (currentI < currentEntities.length) {
        for (let i = currentI; i < currentEntities.length; i++) {
            changes.push(buildEntityChange({
                currentEntity: currentEntities[i],
                comparesTranslations: comparesTranslations
            }));
        }
    }

    return changes;

}

function buildEntityChange(
    { previousEntity = null, currentEntity = null,
        comparesTranslations }
        : {
            previousEntity?: ArrayEntity | null;
            currentEntity?: ArrayEntity | null;
            comparesTranslations: boolean;
        }): EntityChange {

    if (previousEntity !== null && currentEntity !== null) {
        return {
            index: currentEntity.index,
            action: Action.Modified,
            previous: {
                lineNumberInSection: previousEntity.lineNumberInSection,
                originalText: previousEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(previousEntity.originalText, previousEntity.translatedText)
                    : undefined,
            },
            current: {
                lineNumberInSection: currentEntity.lineNumberInSection,
                originalText: currentEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(currentEntity.originalText, currentEntity.translatedText)
                    : undefined,
            },
        };
    } else if (previousEntity != null) {
        return {
            index: previousEntity.index,
            action: Action.Removed,
            previous: {
                lineNumberInSection: previousEntity.lineNumberInSection,
                originalText: previousEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(previousEntity.originalText, previousEntity.translatedText)
                    : undefined,
            },
        };
    } else if (currentEntity != null) {
        return {
            index: currentEntity.index,
            action: Action.Added,
            current: {
                lineNumberInSection: currentEntity.lineNumberInSection,
                originalText: currentEntity.originalText,
                translatedText: comparesTranslations
                    ? getBIfANotEqualToB(currentEntity.originalText, currentEntity.translatedText)
                    : undefined,
            },
        };
    }
    throw new Error("unreachable");
}
