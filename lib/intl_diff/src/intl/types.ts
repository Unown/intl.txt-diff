"use strict";

export class INTL {
    public sections: Section[];

    public constructor(sections: Section[]) {
        this.sections = sections;
    }
}

export enum SectionType {
    ArraySection,
    DictSection
};

export class Section {

    public type: SectionType;

    public id: string;

    public lineNumber: number;

    public entities: ArrayEntity[] | DictEntity[]

    public constructor(type: SectionType, id: string,
        lineNumber: number, entities: ArrayEntity[] | DictEntity[]) {
        this.type = type;
        this.id = id;
        this.lineNumber = lineNumber;
        this.entities = entities;
    }

    public isArraySection(): boolean {
        return this.type == SectionType.ArraySection;
    }
    public isDictSection(): boolean {
        return !this.isArraySection();
    }

    public absoluteLineNumberOf(entity: ArrayEntity | DictEntity) {
        return this.lineNumber + entity.lineNumberInSection - 1;
    }

    public isMapSection(): boolean {
        return Section.isSectionIDForMapSection(this.id);
    }

    public static isSectionIDForMapSection(id: string): boolean {
        return /Map\d+/.test(id);
    }

    public getIDNumberPart(): number | undefined {
        if (this.isMapSection()) {
            return Number(this.id.substring(3));
        }
        const num = Number(this.id);
        if (isNaN(num)) {
            return undefined;
        }
        return num;
    }
}


export class ArrayEntity {
    public lineNumberInSection: number;

    public index: number;
    public originalText: string;
    public translatedText: string;

    public constructor(lineNumberInsection: number, index: number, originalText: string, translatedText: string) {
        this.lineNumberInSection = lineNumberInsection;
        this.index = index;
        this.originalText = originalText;
        this.translatedText = translatedText;
    }
}

export class DictEntity {
    public lineNumberInSection: number;

    public originalText: string;
    public translatedText: string;

    public constructor(lineNumberInsection: number, originalText: string, translatedText: string) {
        this.lineNumberInSection = lineNumberInsection;
        this.originalText = originalText;
        this.translatedText = translatedText;
    }
}
