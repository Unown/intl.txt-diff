"use strict";

import { INTL, Section, SectionType, ArrayEntity, DictEntity } from './types';

enum ParseState {
    ExpectSectionLabel,
    ExpectArrayIndex,
    ExpectOriginalText,
    ExpectTranslation,
}

export function parseINTL(intlText: string): INTL {
    const lines = splitLines(intlText);

    const sections: Section[] = [];

    let state = ParseState.ExpectSectionLabel;

    let currentSectionID: string;
    let currentSectionType: SectionType | null = null;

    let currentSectionEntities: (ArrayEntity | DictEntity)[];//ArrayEntity[] | DictEntity[];
    let currentSectionLineNumber: number;

    let currentLineNumberInSection = 0;
    let currentArrayIndex: number | undefined;
    let currentOriginalText: string;

    for (let lineIndex = 0; ; lineIndex++) {
        const currentLineNumber = lineIndex + 1;

        // line === null means EOF
        const line = (lineIndex === lines.length) ? null : lines[lineIndex]!;

        currentLineNumberInSection++;
        if (line !== null && (
            // it seems empty lines are ignored, 
            // but I'm not sure if lines with spaces are also the case. 
            // I should read relatived source code when possible
            line === ""
            || line.startsWith("#"))) {
            // skip comment line
            continue;
        }

        const _canStartNewSection = canStartNewSection(currentSectionType, state);
        if (line === null && !_canStartNewSection) {
            throw new Error(`line ${currentLineNumber}: unexpected end of file`);
        }

        const willStartNewSection = line !== null && _canStartNewSection && line.startsWith("[");
        if (line === null || willStartNewSection) {
            const _line = line?.trim();
            if (willStartNewSection && !_line!.endsWith("]")) {
                throw new Error(`line ${currentLineNumber}: bad section label: ${line}`);
            }

            // store previous section if has previous section
            if (state != ParseState.ExpectSectionLabel) {
                const newSection = new Section(currentSectionType!,
                    currentSectionID!,
                    currentSectionLineNumber!,
                    currentSectionEntities!);
                sections.push(newSection);

                if (line === null) {
                    return new INTL(sections);
                }
            }

            currentSectionLineNumber = currentLineNumber;
            currentSectionID = _line!.substring(1, _line!.length - 1);
            try {
                currentSectionType = getSectionTypeFromSectionID(currentSectionID);
            } catch (e) {
                if (e instanceof Error) {
                    throw new Error(`line ${currentLineNumber}: ${e.message}`)
                } else {
                    throw new Error(`line ${currentLineNumber}: ${e}`)
                }
            }

            currentLineNumberInSection = 1;
            currentSectionEntities = []
            switch (currentSectionType) {
                case SectionType.ArraySection:
                    state = ParseState.ExpectArrayIndex;
                    break;
                case SectionType.DictSection:
                    state = ParseState.ExpectOriginalText;
                    break;
                case null:
                // unreachable
            }

            continue;
        }

        switch (state) {
            case ParseState.ExpectSectionLabel:
                // unreachable
                break;
            case ParseState.ExpectArrayIndex:
                currentArrayIndex = Number(line);
                if (isNaN(currentArrayIndex)) {
                    throw new Error(`line ${currentLineNumber}: bad array index: ${line}`);
                }
                state = ParseState.ExpectOriginalText;
                break;
            case ParseState.ExpectOriginalText:
                currentOriginalText = line;
                state = ParseState.ExpectTranslation;
                break;
            case ParseState.ExpectTranslation:
                switch (currentSectionType) {
                    case SectionType.ArraySection:
                        {
                            const newArrayEntity = new ArrayEntity(currentLineNumberInSection - 2, currentArrayIndex!, currentOriginalText!, line);
                            currentSectionEntities!.push(newArrayEntity);
                            state = ParseState.ExpectArrayIndex;
                        }
                        break;
                    case SectionType.DictSection:
                        {
                            const newDictEntity = new DictEntity(currentLineNumberInSection - 1, currentOriginalText!, line);
                            currentSectionEntities!.push(newDictEntity);
                            state = ParseState.ExpectOriginalText;
                        }
                        break;
                    case null:
                    // unreachable
                }
                break;
        }
    }

}

function splitLines(text: string): string[] {
    return text.split(/\r?\n/);
}

function canStartNewSection(sectionType: SectionType | null, state: ParseState): boolean {
    switch (sectionType) {
        case null:
            return state === ParseState.ExpectSectionLabel;
        case SectionType.ArraySection:
            return state === ParseState.ExpectArrayIndex;
        case SectionType.DictSection:
            return state === ParseState.ExpectOriginalText;
    }
}

function getSectionTypeFromSectionID(id: string): SectionType {
    if (Section.isSectionIDForMapSection(id)) {
        return SectionType.DictSection;
    }

    const idNumber = Number(id);
    if (isNaN(idNumber)) {
        throw new Error(`bad section label: ${id}`);
    }

    const table: { [id: string]: SectionType | undefined } = {
        1: SectionType.ArraySection,
        2: SectionType.ArraySection,
        3: SectionType.ArraySection,
        4: SectionType.ArraySection,
        5: SectionType.ArraySection,
        6: SectionType.ArraySection,
        7: SectionType.ArraySection,
        8: SectionType.ArraySection,
        9: SectionType.ArraySection,
        10: SectionType.ArraySection,
        11: SectionType.ArraySection,
        12: SectionType.ArraySection,
        13: SectionType.DictSection,
        14: SectionType.DictSection,
        15: SectionType.DictSection,
        16: SectionType.DictSection,
        17: SectionType.ArraySection,
        18: SectionType.DictSection,
        19: SectionType.DictSection,
        20: SectionType.ArraySection,
        21: SectionType.DictSection,
        22: SectionType.DictSection,
    };

    const type = table[idNumber];
    if (type === undefined) {
        throw new Error(`unknown section: section ${idNumber}`);
    }
    return type;

}