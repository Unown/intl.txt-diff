import { parseINTL, INTL } from "./intl/index";

import { INTLChanges } from "./diff/types";
import { compareSectionLists } from "./diff/section-lists";

export { diff, INTLChanges };

function diff(previousINTLText: string, currentINTLText: string, comparesTranslations: boolean): INTLChanges {

    let previousINTL: INTL;
    let currentINTL: INTL;
    let isPrevisousINTLParsed = false;
    try {
        previousINTL = parseINTL(previousINTLText);
        isPrevisousINTLParsed = true;
        currentINTL = parseINTL(currentINTLText);
    } catch (e) {
        const which = isPrevisousINTLParsed ? "newer" : "older";
        if (e instanceof Error) {
            throw new Error(`failed to parse the ${which} intl file: ${e.message}`);
        } else {
            throw new Error(`failed to parse the ${which} intl file: ${e}`);
        }
    }

    return compareSectionLists(previousINTL.sections, currentINTL.sections, comparesTranslations);

}

