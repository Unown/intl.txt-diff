import { Action, SectionChanges } from "../../intl_diff/src/diff/types";

export function moveInnerNodes(src: HTMLElement, dst: HTMLElement) {
    while (src.childNodes.length > 0) {
        dst.appendChild(src.childNodes[0]);
    }
}

export function getTemplateContentFirstChildCloneById(id: string): HTMLElement {
    const template =
        <HTMLTemplateElement>document.getElementById(id)!;
    return <HTMLElement>template.content.children[0].cloneNode(true);
}

export function getActionSymbol(action: Action): string {
    switch (action) {
        case Action.Added:
            return "+";
        case Action.Removed:
            return "-";
        case Action.Modified:
            return "Δ";
    }
}

export function getActionClassName(action: Action): string {
    switch (action) {
        case Action.Added:
            return "added-content";
        case Action.Removed:
            return "removed-content";
        case Action.Modified:
            return "modified-content";
    }
}

export function getSectionAnchorName(section: SectionChanges): string {
    if (/Map\d+/.test(section.id)) {
        return `section-map-${Number(section.id.substring(3))}`;
    } else if (!isNaN(Number(section.id))) {
        return `section-${section.id}`;
    }
    throw new Error(`unknown section id: ${section.id}`);
}

export function getRegularSectionName(section: SectionChanges): string | undefined {
    return sectionNameMap.get(section.id);
}

const sectionNameMap = new Map<string, string>(Object.entries({
    "1": "Species",
    "2": "Kinds",
    "3": "Entries",
    "4": "FormNames",
    "5": "Moves",
    "6": "MoveDescriptions",
    "7": "Items",
    "8": "ItemDescriptions",
    "9": "Abilities",
    "10": "AbilityDescs",
    "11": "Types",
    "12": "TrainerTypes",
    "13": "TrainerNames",
    "14": "BeginSpeech",
    "15": "EndSpeechWin",
    "16": "EndSpeechLose",
    "17": "RegionNames",
    "18": "PlaceNames",
    "19": "PlaceDescriptions",
    "20": "MapNames",
    "21": "PhoneMessages",
    "22": "ScriptTexts",
})); 