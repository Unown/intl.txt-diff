import { SectionType } from "../../intl_diff/src/intl/types";
import { INTLChanges, SectionChanges, EntityChange } from "../../intl_diff/src/diff/types";

import { Info } from "./rendering_changes";
import { getRegularSectionName, getSectionAnchorName, getActionClassName, getActionSymbol, moveInnerNodes, getTemplateContentFirstChildCloneById } from "./utils";

export function renderChangesAsRichOutput(info: Info,
    { removedSections, addedSections, modifiedSections }: INTLChanges): HTMLElement {
    const output = <HTMLDivElement>
        getTemplateContentFirstChildCloneById("rich-output-template");

    for (const [className, sections, shouldLink] of [
        ["removed-sections-div", removedSections, false],
        ["added-sections-div", addedSections, false],
        ["modified-sections-div", modifiedSections, true],
    ] as unknown as [[string, SectionChanges[], boolean]]) {
        const sectionContainer =
            output.getElementsByClassName(className)[0];
        if (sections.length === 0) {
            sectionContainer.remove();
            continue;
        }

        const listContainerOutput = renderSectionList(sections, shouldLink);
        const listContainer = <HTMLDivElement>sectionContainer
            .getElementsByClassName("list-div")[0];
        moveInnerNodes(listContainerOutput, listContainer);
    }

    const modifiedSectionsContainerOutput = renderSectionChanges(info, modifiedSections);
    const modifiedSectionsContainer = <HTMLDivElement>
        output.getElementsByClassName("modified-sections-changes")[0];
    moveInnerNodes(modifiedSectionsContainerOutput, modifiedSectionsContainer);

    return output;

}

function renderSectionList(sections: SectionChanges[], shouldLink: boolean): HTMLDivElement {
    const sectionListWrapper = <HTMLDivElement>
        getTemplateContentFirstChildCloneById("section-list-template");
    const mapSectionList =
        sectionListWrapper.getElementsByClassName("map-section-list")[0];
    const regularSectionList =
        sectionListWrapper.getElementsByClassName("regular-section-list")[0];

    for (const section of sections) {
        const isMapSection = section.id.startsWith("Map");

        let label = `[${section.id}]`;
        if (!isMapSection) {
            label += ` # ${getRegularSectionName(section)}`;
        }

        const span = document.createElement("span");
        if (shouldLink) {
            const a = document.createElement("a");
            a.innerText = label;
            a.href = `#${getSectionAnchorName(section)}-changes`;
            span.appendChild(a);
        } else {
            span.innerText = label;
        }

        if (isMapSection) {
            if (mapSectionList.children.length > 0) {
                mapSectionList.appendChild(document.createTextNode(" "));
            }
            mapSectionList.appendChild(span);
        } else {
            if (regularSectionList.children.length > 0) {
                regularSectionList.appendChild(document.createElement("br"));
            }
            regularSectionList.appendChild(span);
        }

    }

    return sectionListWrapper;

}

function renderSectionChanges(info: Info, sections: SectionChanges[]): HTMLDivElement {
    const container = document.createElement("div");

    for (const section of sections) {

        const table = renderSectionChangesTable(info, section);

        for (const change of section.changes!) {
            const changeRow = renderSectionEntityChange(info, section, change);
            table.tBodies[0].appendChild(changeRow);
        }

        container.appendChild(table);
    }

    return container;

}

function renderSectionChangesTable(info: Info, section: SectionChanges): HTMLTableElement {
    const table = <HTMLTableElement>
        getTemplateContentFirstChildCloneById("section-changes-table-template");

    table.id = `${getSectionAnchorName(section)}-changes`;

    const titleTH = <HTMLTableHeaderCellElement>
        table.getElementsByClassName("title")[0];
    const sectionName = getRegularSectionName(section);
    if (sectionName === undefined) {
        titleTH.innerText = `[${section.id}]`;
    } else {
        titleTH.innerText = `[${section.id}] # ${sectionName}`;
    }

    if (section.type !== SectionType.ArraySection) {
        // table.getElementsByClassName("index-head")[0].style = "visibility: collapse;";
        table.getElementsByClassName("index-head")[0].remove();
    }

    if (info.olderFileName === info.newerFileName) {
        (<HTMLTableHeaderCellElement>
            table.getElementsByClassName("older-file-label")[0])
            .innerText = `${info.olderFileName} (older)`;
        (<HTMLTableHeaderCellElement>
            table.getElementsByClassName("newer-file-label")[0])
            .innerText = `${info.newerFileName} (newer)`;
    } else {
        (<HTMLTableHeaderCellElement>
            table.getElementsByClassName("older-file-label")[0])
            .innerText = info.olderFileName;
        (<HTMLTableHeaderCellElement>
            table.getElementsByClassName("newer-file-label")[0])
            .innerText = info.newerFileName;
    }

    return table;

}

function renderSectionEntityChange(info: Info, section: SectionChanges, change: EntityChange) {
    const changeRow = <HTMLTableRowElement>
        getTemplateContentFirstChildCloneById("section-changes-table-change-row-template");

    changeRow.classList.add(getActionClassName(change.action));

    const actionLabelTD = <HTMLTableDataCellElement>
        changeRow.getElementsByClassName("operation-label")[0];
    actionLabelTD.innerText = getActionSymbol(change.action);

    const indexTD = <HTMLTableDataCellElement>
        changeRow.getElementsByClassName("index")[0];
    if (section.type === SectionType.ArraySection) {
        indexTD.innerText = String(change.index);
    } else {
        // indexTD.style = "visibility: collapse;";
        indexTD.remove();
    }

    if (change.previous !== undefined) {
        const olderLineNumberTD = <HTMLTableDataCellElement>
            changeRow.getElementsByClassName("older-line-number")[0];
        olderLineNumberTD.appendChild(
            renderLineNumberFragment(section.previousLineNumber!, change.previous.lineNumberInSection));
        const olderContentTD = <HTMLTableDataCellElement>
            changeRow.getElementsByClassName("older-content")[0];
        moveInnerNodes(renderContent(info, change, true), olderContentTD);
    }
    if ((change.current !== undefined)) {
        const newerLineNumberTD = changeRow.getElementsByClassName("newer-line-number")[0];
        newerLineNumberTD.appendChild(
            renderLineNumberFragment(section.currentLineNumber!, change.current.lineNumberInSection));
        const newerContentTD = <HTMLTableDataCellElement>
            changeRow.getElementsByClassName("newer-content")[0];
        moveInnerNodes(renderContent(info, change, false), newerContentTD);
    }

    return changeRow;

}

function renderContent(info: Info, change: EntityChange, isPrevious: boolean): HTMLDivElement {

    const originalText = isPrevious
        ? change.previous!.originalText
        : change.current!.originalText;
    const translatedText = info.comparedTranslations
        ? (isPrevious
            ? change.previous!.translatedText
            : change.current!.translatedText)
        : null;
    const shouldLowlightOriginalText = info.comparedTranslations
        ? (change.previous?.originalText
            === change.current?.originalText)
        : false;

    let content: HTMLDivElement;
    if (translatedText === null) {
        content = <HTMLTableRowElement>
            getTemplateContentFirstChildCloneById("section-changes-table-change-row-simple-content-template");
    } else {
        content = <HTMLTableRowElement>
            getTemplateContentFirstChildCloneById("section-changes-table-change-row-complex-content-template");
    }

    const originalContentDiv = <HTMLDivElement>
        content.getElementsByClassName("original-content")[0];
    originalContentDiv.innerText = originalText;
    if (shouldLowlightOriginalText) {
        originalContentDiv.classList.add("lowlight");
    }

    if (translatedText === null) {
        return content;
    }

    const shouldLowlightTranslatedText =
        change.previous?.translatedText
        === change.current?.translatedText;

    const translatedContentDiv = <HTMLDivElement>
        content.getElementsByClassName("translated-content")[0];
    translatedContentDiv.innerText = translatedText!;
    if (shouldLowlightTranslatedText) {
        translatedContentDiv.classList.add("lowlight");
    }

    return content;

}

function renderLineNumberFragment(
    sectionLineNumber: number, lineNumberInSection: number)
    : DocumentFragment {
    const fragment = document.createDocumentFragment();

    const absoluteLineNumber = sectionLineNumber + lineNumberInSection - 1;
    fragment.appendChild(document.createTextNode(String(absoluteLineNumber)));

    fragment.appendChild(document.createElement("br"))

    const span = document.createElement("span");
    span.innerText = `(${lineNumberInSection})`
    span.style.fontSize = "small";
    fragment.appendChild(span);

    return fragment;

}