import { INTLChanges } from "../../intl_diff/src/diff/types";

import { renderChangesAsRichOutput } from "./rich-output";
import { renderChangesAsTextOutput } from "./text-output";

export interface Info {
    olderFileName: string;
    newerFileName: string;
    comparedTranslations: boolean;
}

export interface Options {
    usesRichOutput: boolean;
}

export function renderChanges(
    info: Info, changes: INTLChanges, options: Options): HTMLElement {

    if (options.usesRichOutput) {
        return renderChangesAsRichOutput(info, changes);
    } else {
        return renderChangesAsTextOutput(info, changes);
    }

}