import { INTLChanges } from "../../intl_diff/src/diff/types";

import { Info } from "./rendering_changes";
import { getActionSymbol } from "./utils";

export function renderChangesAsTextOutput(info: Info,
    { removedSections, addedSections, modifiedSections }: INTLChanges)
    : HTMLElement {

    if (info.comparedTranslations) {
        throw new Error("not supported");
    }

    let output = "";

    if (removedSections.length > 0) {
        for (const section of removedSections) {
            output += `[${section.id}]\n`;
        }
        output += "\n\n\n\n";
    }

    if (addedSections.length > 0) {
        output += "ADDED SECTIONS:\n";
        for (const section of addedSections) {
            output += `[${section.id}]\n`;
        }
        output += "\n\n\n\n";
    }

    if (modifiedSections.length > 0) {
        output += "MODIFIED SECTIONS:\n";
        output += "// FORMAT: (+/-/Δ|LINE NUMBER IN FILE|LINE NUMBER IN SECTION):TEXT\n";
        output += "\n\n";

        for (const section of modifiedSections) {
            output += `[${section.id}]\n`;

            for (const change of section.changes!) {
                let symbol = getActionSymbol(change.action);

                let [absoluteLineNumber, relativeLineNumber] = ["", ""];
                if (change.previous !== undefined) {
                    absoluteLineNumber += `${section.previousLineNumber! + change.previous.lineNumberInSection - 1}`;
                    relativeLineNumber += `${change.previous.lineNumberInSection}`;
                }
                if (change.previous !== undefined && change.current !== undefined) {
                    absoluteLineNumber += "→";
                    relativeLineNumber += "→";
                }
                if (change.current !== undefined) {
                    absoluteLineNumber += `${section.currentLineNumber! + change.current.lineNumberInSection - 1}`;
                    relativeLineNumber += `${change.current.lineNumberInSection}`;
                }

                let content = "";
                if (change.index !== undefined) {
                    content += `${change.index}:`;
                }
                if (change.previous !== undefined) {
                    content += `${change.previous.originalText}`;
                }
                if (change.previous !== undefined && change.current !== undefined) {
                    content += "→"
                }
                if (change.current !== undefined) {
                    content += `${change.current.originalText}`;
                }

                output += `(${symbol}|${absoluteLineNumber}|${relativeLineNumber}):${content}\n`;

            }

            output += "\n\n";

        }

    }

    const textarea = document.createElement("textarea");
    textarea.readOnly = true;
    textarea.value = output;

    return textarea;

}